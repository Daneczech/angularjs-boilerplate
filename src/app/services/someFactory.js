/*
|| What should I Eat Tonight?
||---------------------------
|| Author: Dan Dvoracek
|| Description: Can be made available accross all views by injecting in the controller params
*/

(function() {

	var someFactory = function($q) {

		var factory = {},
			randomNum;
		
		factory.getRandomNum = function(){
			
			randomNum = Math.random();

			return randomNum;
		}

		return factory;

	};

	angular.module('someApp').factory('someFactory', someFactory);

}());