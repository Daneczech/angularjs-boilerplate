/*
|| What should I Eat Tonight?
||---------------------------
|| Author: Dan Dvoracek
|| Description: Values to be shared across the website
*/
angular.module('someApp').value('appSettings', {
	title: 'This is an app',
	version: '0.1'
});