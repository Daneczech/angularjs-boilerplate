/*
|| What should I Eat Tonight?
||---------------------------
|| Author: Dan Dvoracek
|| Description: Makes the recipes available to its scope 
*/

(function() {

	var BaseController = function($scope, someFactory) {
		// "Init function": Get a first random number onload by executing it straight away
		$scope.getSomeNum = someFactory.getRandomNum();

	};

	BaseController.$inject = ['$scope', 'someFactory'];

	angular.module('someApp').controller('BaseController', BaseController);

}());