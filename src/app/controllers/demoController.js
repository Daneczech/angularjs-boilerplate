/*
|| What should I Eat Tonight?
||---------------------------
|| Author: Dan Dvoracek
|| Description: Makes the recipes available to its scope 
*/

(function() {

	var DemoController = function ($scope, someFactory) {
		
		$scope.getSomeNum = '';

		function init() {
			// "Init function": Get a first random number onload by executing it straight away
			$scope.getSomeNum = someFactory.getRandomNum();
		}

		init();
	}

	DemoController.$inject = ['$scope', 'someFactory'];

	angular.module('someApp').controller('DemoController', DemoController);

}());