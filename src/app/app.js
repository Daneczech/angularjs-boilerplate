/*
|| What should I Eat Tonight?
||---------------------------
|| Author: Dan Dvoracek
|| Description: App declaration & routing
|| Views are outside the src folder as we won't push it live when times come..
*/

(function() {
    
    /*-- Register the app --*/
    var app = angular.module('someApp', ['ngRoute']);
    
    /*-- Setting up the routes --*/
    app.config(function($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'BaseController',
                templateUrl: 'views/home.html'
            })
            .when('/demo', {
                controller: 'DemoController',
                templateUrl: 'views/demo.html'
            })
            .otherwise( { redirectTo: '/' } );
    });
    
}());