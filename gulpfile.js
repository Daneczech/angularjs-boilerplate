// include gulp
var gulp = require('gulp'); 

// include plug-ins
var compass = require('gulp-compass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');


// Compass task - loaded from the config.rb
gulp.task('compass', function() {
  gulp.src('./src/sass/**/*.scss')
    .pipe(compass({
      config_file: './config.rb',
      css: './stylesheets',
      sass: './src/sass'
    }))
    .pipe(gulp.dest('temp'));
});

//--- scripts concatenation
gulp.task('scripts', function() {
  return gulp.src('./src/app/**/*.js')
    .pipe(concat('main.js'))
    .pipe(uglify({mangle: false}))
    .pipe(gulp.dest('./scripts/'));
});

//--- gulp
gulp.task('default', ['compass', 'scripts'], function() {});

//--- gulp watch
gulp.task('watch', function () {
	gulp.watch('./src/sass/**/*.scss', ['compass']);
  gulp.watch('./src/app/**/*.js', ['scripts']);
});