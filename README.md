# AngularJS - Boilerplate #

This is a very simple and basic boilerplate for any AngularJS app.
For good sake, this boilerplate features Compass-Sass, Susy and Gulp.

### How to set up? ###

* Clone the repo
* Install dependencies: $ npm install (or $ sudo npm install)
* Gulp tasks: gulp // gulp watch

When running the gulp tasks, the js will get concatenated and uglified. If you want this behaviour to change, feel free to tweak the Gulpfile.js file.

### Structure Tips ###

The app has a **./src/** folder that contains all the sass and js code for development.

Once gulp is done, the sass is compiled in the **./stylesheets/** folder and the js files are concatenated and uglified into one main.js in **./scripts/** .

Views remain in a separated folder at the root (**./views/**).